'use strict';
var Duel = (function () {
    function Duel(players) {
        this.players = players;
        this.turns = [];
    }
    Duel.prototype.isReady = function () {
        var ready = true;
        this.players.forEach(function (p) {
            if (!p.faction) {
                ready = false;
            }
        });
        return ready;
    };
    ;
    Duel.prototype.hasOrders = function (turn) {
        return this.turns.length == (turn + 1);
    };
    ;
    Duel.prototype.hasAllOrdersForTurn = function (turn) {
        return this.turns.length == (turn + 1) && this.turns[turn].length == this.players.length;
    };
    ;
    Duel.prototype.addOrders = function (turn, playerToken, orders) {
        this.turns[turn].push({
            player: playerToken,
            orders: orders
        });
    };
    ;
    Duel.prototype.addTurn = function () {
        this.turns.push([]);
    };
    return Duel;
}());
exports.Duel = Duel;
//# sourceMappingURL=Duel.js.map