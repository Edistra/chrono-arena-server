'use strict';
var WebSocket = require('ws');
var models = require('./models');
var Player = require('./Player');
var Duel = require('./Duel');
var port = process.env.PORT || 3000;
var WebSocketServer = WebSocket.Server;
var server = new WebSocketServer({ port: port });
var playersList = [];
var duelsList = [];
server.on('connection', function (ws) {
    ws.on('message', function (message) {
        try {
            var msg = new models.Message(JSON.parse(message));
            console.log(msg);
            var targetClient = getClientFromToken(msg.content);
            var targetPlayer = getPlayerFromToken(msg.content);
            var askingPlayer = getPlayerFromToken(msg.name);
            var duel = null;
            if (askingPlayer !== null) {
                duel = getDuelFromToken(askingPlayer.token);
                console.log(duel);
            }
            if (msg.type == 'NEW_CONNECTION') {
                var token = generateUniqueToken(playersList);
                playersList.push(new Player.Player(token, msg.name));
                ws.id = token;
                emit(getMessage('SERVER_TOKEN', token), [ws]);
                broadcast(getMessage('SERVER_NOTIFICATION', msg.name + ' vient de rentrer dans le lobby'));
                broadcast(getMessage('SERVER_PLAYERS_LIST', playersList));
            }
            else if (msg.type == 'ASK_DUEL') {
                emit(getMessage('ASK_DUEL', askingPlayer.name + ' vous provoque en duel', askingPlayer.token), [targetClient]);
            }
            else if (msg.type == 'DECLINE_DUEL') {
                emit(getMessage('DECLINE_DUEL', askingPlayer.name + ' a refusé le duel', askingPlayer.token), [targetClient]);
            }
            else if (msg.type == 'ACCEPT_DUEL') {
                askingPlayer.master = true;
                duelsList.push(new Duel.Duel([askingPlayer, targetPlayer]));
                emit(getMessage('ACCEPT_DUEL', askingPlayer.name + ' a accepté le duel', askingPlayer.token), [targetClient]);
            }
            else if (msg.type == 'FACTION_CHOSEN') {
                askingPlayer.faction = msg.content;
                if (duel.isReady()) {
                    emit(getMessage('START_DUEL', duel), getClientsFromPlayers(duel.players));
                }
            }
            else if (msg.type == 'VALID_ORDER_PHASE') {
                var data = msg.content;
                if (!duel.hasOrders(data.turn)) {
                    duel.addTurn();
                }
                duel.addOrders(data.turn, askingPlayer.token, data.orders);
                if (duel.hasAllOrdersForTurn(data.turn)) {
                    console.log('oki doki');
                    var clients = getClientsFromPlayers(duel.players.filter(function (p) { return p.master; }));
                    console.log(clients.length);
                    emit(getMessage('PROCESS_ORDERS', duel.turns[data.turn]), clients);
                }
            }
            else if (msg.type == 'PROCESSED_ORDERS') {
                emit(getMessage('PROCESSED_ORDERS', msg.content), getClientsFromPlayers(duel.players));
            }
            else if (msg.type == 'KEEP_ALIVE') {
                console.log('keep_alive received from ' + msg.name);
            }
            else {
                broadcast(msg);
            }
        }
        catch (e) {
            console.error(e, e.message);
        }
    });
    ws.on('close', function () {
        console.log('disconnect');
        var p = getPlayerFromToken(ws.id);
        if (p !== null && p.name) {
            broadcast(getMessage('SERVER_NOTIFICATION', p.name + ' vient de quitter le lobby'));
        }
        removePlayer(ws.id);
        broadcast(getMessage('SERVER_PLAYERS_LIST', playersList));
    });
});
function getClientsFromPlayers(players) {
    var result = [];
    players.forEach(function (p) {
        result.push(getClientFromToken(p.token));
    });
    return result;
}
;
function getMessage(type, content, name) {
    if (name === void 0) { name = 'Server'; }
    return new models.Message({
        'name': name,
        'content': content,
        'type': type
    });
}
;
function getDuelFromToken(token) {
    var result = null;
    duelsList.forEach(function (d) {
        d.players.forEach(function (p) {
            if (p.token == token) {
                result = d;
            }
        });
    });
    return result;
}
;
function getPlayerFromToken(token) {
    var result = null;
    playersList.forEach(function (p) {
        if (p.token == token) {
            result = p;
        }
    });
    return result;
}
function getClientFromToken(token) {
    var result = null;
    server.clients.forEach(function (c) {
        if (c.id == token) {
            result = c;
        }
    });
    return result;
}
function removePlayer(token) {
    var temp = [];
    playersList.forEach(function (p) {
        if (p.token != token) {
            temp.push(p);
        }
    });
    playersList = temp;
}
function send(client, data) {
    client.send(JSON.stringify(data), function (error) {
        console.error(error);
    });
}
;
function broadcast(data) {
    server.clients.forEach(function (c) {
        send(c, data);
    });
}
;
function emit(data, clients) {
    clients.forEach(function (c) {
        send(c, data);
    });
}
;
function generateUniqueToken(list) {
    var unique = false;
    var token = '';
    while (!unique) {
        unique = true;
        token = 'token' + new Date().getTime();
        Object.keys(list).forEach(function (key) {
            if (token == key)
                unique = false;
        });
    }
    return token;
}
;
console.log('Server is running on port', port);
//# sourceMappingURL=server.js.map