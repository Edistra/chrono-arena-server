'use strict';
var Player = (function () {
    function Player(token, name, faction, master) {
        if (faction === void 0) { faction = ''; }
        if (master === void 0) { master = false; }
        this.token = token;
        this.name = name;
        this.faction = faction;
        this.master = master;
    }
    return Player;
}());
exports.Player = Player;
//# sourceMappingURL=Player.js.map