'use strict';
var Message = (function () {
    function Message(data) {
        if (!data.name || !data.content) {
            throw new Error('Invalid content data received: ' + data);
        }
        this.data = data;
    }
    Object.defineProperty(Message.prototype, "type", {
        get: function () {
            return this.data.type;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Message.prototype, "name", {
        get: function () {
            return this.data.name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Message.prototype, "content", {
        get: function () {
            return this.data.content;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Message.prototype, "action", {
        get: function () {
            return this.data.action;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Message.prototype, "extra", {
        get: function () {
            return this.data.extra;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Message.prototype, "get_data", {
        get: function () {
            return this.data;
        },
        enumerable: true,
        configurable: true
    });
    Message.prototype.setExtra = function (extra) {
        this.data.extra = extra;
    };
    return Message;
}());
exports.Message = Message;
//# sourceMappingURL=models.js.map