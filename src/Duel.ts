'use strict';

export class Duel {
    players;
    turns;

    constructor(players) {
        this.players = players;
        this.turns = [];
    }

    isReady() {
        let ready = true;
        this.players.forEach(p => {
            if (!p.faction) {
                ready = false;
            }
        });
        return ready;
    };

    hasOrders(turn) {
        return this.turns.length == (turn + 1);
    };


    hasAllOrdersForTurn(turn) {
        return this.turns.length == (turn + 1) && this.turns[turn].length == this.players.length;
    };

    addOrders(turn, playerToken, orders) {
        this.turns[turn].push({
            player: playerToken,
            orders: orders
        });
    };

    addTurn() {
        this.turns.push([]);
    }
}