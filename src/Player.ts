'use strict';

export class Player {
    token: string;
    name: string;
    faction: string;
    master: boolean;

    constructor(token, name, faction='', master=false) {
        this.token = token;
        this.name = name;
        this.faction = faction;
        this.master = master;
    }
}