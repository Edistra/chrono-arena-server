/// <reference path='../typings/globals/node/index.d.ts' />
/// <reference path='../typings/modules/ws/index.d.ts' />
'use strict';

import WebSocket = require('ws');
import models = require('./models');
import Player = require('./Player');
import Duel = require('./Duel');

var port:number = process.env.PORT || 3000;
var WebSocketServer = WebSocket.Server;
var server = new WebSocketServer({port: port});
var playersList:Player.Player[] = [];
var duelsList:Duel.Duel[] = [];

server.on('connection', ws => {
    ws.on('message', message => {
        try {
            var msg:models.Message = new models.Message(JSON.parse(message));
            console.log(msg);
            let targetClient = getClientFromToken(msg.content);
            let targetPlayer = getPlayerFromToken(msg.content);
            let askingPlayer = getPlayerFromToken(msg.name);
            let duel = null;
            if (askingPlayer !== null) {
                duel = getDuelFromToken(askingPlayer.token);
                console.log(duel);
            }

            if (msg.type == 'NEW_CONNECTION') {
                let token = generateUniqueToken(playersList);
                playersList.push(new Player.Player(token, msg.name));
                (ws as any).id = token;
                emit(getMessage('SERVER_TOKEN', token), [ws]);
                broadcast(getMessage('SERVER_NOTIFICATION', msg.name + ' vient de rentrer dans le lobby'));
                broadcast(getMessage('SERVER_PLAYERS_LIST', playersList));
            } else if (msg.type == 'ASK_DUEL') {
                emit(getMessage('ASK_DUEL', askingPlayer.name + ' vous provoque en duel', askingPlayer.token), [targetClient]);
            } else if (msg.type == 'DECLINE_DUEL') {
                emit(getMessage('DECLINE_DUEL', askingPlayer.name + ' a refusé le duel', askingPlayer.token), [targetClient]);
            } else if (msg.type == 'ACCEPT_DUEL') {
                askingPlayer.master = true;
                duelsList.push(new Duel.Duel([askingPlayer, targetPlayer]));
                // TODO Avoid player to be in multiple duels at the same time
                emit(getMessage('ACCEPT_DUEL', askingPlayer.name + ' a accepté le duel', askingPlayer.token), [targetClient]);
            } else if (msg.type == 'FACTION_CHOSEN') {
                askingPlayer.faction = msg.content;
                if (duel.isReady()) { emit(getMessage('START_DUEL', duel), getClientsFromPlayers(duel.players)); }
            } else if (msg.type == 'VALID_ORDER_PHASE') {
                let data = msg.content; // {turn: 1, order: []}
                if (!duel.hasOrders(data.turn)) { duel.addTurn(); }
                duel.addOrders(data.turn, askingPlayer.token, data.orders);
                if(duel.hasAllOrdersForTurn(data.turn)) {
                    console.log('oki doki');
                    let clients = getClientsFromPlayers(duel.players.filter( p => { return p.master; }));
                    console.log(clients.length);
                    emit(getMessage('PROCESS_ORDERS', duel.turns[data.turn]), clients);
                }
            } else if (msg.type == 'PROCESSED_ORDERS') {
                emit(getMessage('PROCESSED_ORDERS', msg.content), getClientsFromPlayers(duel.players));
            }  else if (msg.type == 'KEEP_ALIVE') {
                console.log('keep_alive received from ' + msg.name);
            } else {
                broadcast(msg);
            }
        } catch (e) {
            console.error(e, e.message);
        }
    });

    ws.on('close', function () {
        console.log('disconnect');
        let p = getPlayerFromToken((ws as any).id);
        if(p !== null && p.name) {
            broadcast(getMessage('SERVER_NOTIFICATION', p.name + ' vient de quitter le lobby'));
        }
        removePlayer((ws as any).id);
        broadcast(getMessage('SERVER_PLAYERS_LIST', playersList));
    });
});

function getClientsFromPlayers(players) {
    let result = [];
    players.forEach(p => {
        result.push(getClientFromToken(p.token));
    });
    return result;
};

function getMessage(type, content, name = 'Server'):models.Message {
    return new models.Message({
        'name': name,
        'content': content,
        'type': type
    });
};
function getDuelFromToken(token):Duel.Duel {
    let result = null;
    duelsList.forEach(d => {
        d.players.forEach(p => {
            if (p.token == token) {
                result = d;
            }
        });
    });
    return result;
};
function getPlayerFromToken(token):Player.Player {
    let result = null;
    playersList.forEach(p => {
        if (p.token == token) {
            result = p;
        }
    });
    return result;
}
function getClientFromToken(token) {
    let result = null;
    server.clients.forEach(c => {
        if ((c as any).id == token) {
            result = c;
        }
    });
    return result;
}
function removePlayer(token):void {
    let temp = [];
    playersList.forEach(p => {
        if (p.token != token) {
            temp.push(p);
        }
    });
    playersList = temp;
}

function send(client, data):void {
    client.send(JSON.stringify(data), function (error) {
        console.error(error);
    });
};

function broadcast(data):void {
    server.clients.forEach(c => {
        send(c, data);
    });
};

function emit(data, clients):void {
    clients.forEach(c => {
        send(c, data);
    });
};

function generateUniqueToken(list):string {
    let unique = false;
    let token = '';
    while (!unique) {
        unique = true;
        token = 'token' + new Date().getTime();
        Object.keys(list).forEach(function (key) {
            if (token == key) unique = false;
        });
    }
    return token;
};

console.log('Server is running on port', port);