'use strict';

interface Msg {
    type: string;
    name: string;
    content;
    action: string;
    extra;
}

export class Message implements Msg {
    private data: {
        type: string,
        name: string,
        content: any,
        action: string,
        extra
    };

    constructor(data) {
        if (!data.name || !data.content) {
            throw new Error('Invalid content data received: ' + data);
        }
        this.data = data;
    }

    get type(): string {
        return this.data.type;
    }

    get name(): string {
        return this.data.name;
    }

    get content() {
        return this.data.content;
    }

    get action(): string {
        return this.data.action;
    }

    get extra() {
        return this.data.extra;
    }

    get get_data() {
        return this.data;
    }

    setExtra(extra) {
        this.data.extra = extra;
    }
}